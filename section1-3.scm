(load "section1-2.scm")

; Section 1.3.1

(define (cube x)
  "Compute X^3."
  (* x x x))

(define (sum-integers a b)
  "Compute the sum of the integers from A to B inclusive."
  (if (> a b)
      0
      (+ a (sum-integers (+ a 1) b))))

(define (sum-cubes a b)
  "Compute the sum of the cube of all integers from A to B inclusive."
  (if (> a b)
      0
      (+ (cube a) (sum-cubes (+ a 1) b))))

(define (pi-sum a b)
  "Compute the sum of the series 1/(a*(a+2)) + 1/((a+2)*(a+4)) from A to B inclusive."
  (if (> a b)
      0
      (+ (/ 1.0 (* a (+ a 2))) (pi-sum (+ a 4) b))))

(define-syntax sigma
  (syntax-rules ()
    ((_ name term next)
     (define (name a b)
       (if (> a b)
           0
           (+ (term a)
              (name (next a) b)))))))

(define (sum term a next b)
  "Compute the sum of a series.

TERM is the expression to sum.

A is the first integer to input to the expression.

NEXT is the expression used to determine the next input.

B is the final integer to input to the expression."
  (if (> a b)
      0
      (+ (term a)
         (sum term (next a) next b))))

(define (inc n)
  "Increment number N by 1."
  (+ n 1))

(define (sum-cubes a b)
  "Compute the sum of the series N^3 from A to B inclusive using `sum'."
  (sum cube a inc b))

(define (identity x)
  "Return X."
  x)

(define (sum-integers a b)
  "Compute the sum of integers from A to B inclusive using `sum'."
  (sum identity a inc b))

(define (pi-sum a b)
  "Compute the sum of the series 1/(a*(a+2)) + 1/((a+2)*(a+4)) from A to B inclusive using `sum'."
  (define (pi-term x)
    "Compute 1/(X*(X+2))."
    (/ 1.0 (* x (+ x 2))))
  (define (pi-next x)
    "Compute X+4."
    (+ x 4))
  (sum pi-term a pi-next b))

(define (integral f a b dx)
  "Approximate integral of function.

F is the function.

A is the lower bound.

B is the upper bound.

DX is the change in the input."
  (define (add-dx x)
    "Add X to DX."
    (+ x dx))
  (* (sum f (+ a (/ dx 2.0)) add-dx b)
     dx))

; Exercise 1.29
(define (simpson-integral f a b n)
  "Compute the integral of a function using Simpson's Rule.

F is the function.

A is the lower bound.

B is the upper bound.

N is the number of steps."
  (define h (/ (- b a) n))
  (define (y k)
    "Compute y_k."
    (f (+ a (* k h))))
  (define (term k)
    "Helper function for `simpson-integral'"
    (* (cond ((odd? k) 4)
             ((or (= k 0) (= k n)) 1)
             ((even? k) 2))
       (y k))) 
  (* (/ h 3) (sum term 0 inc n)))
; (integral cube 0 1 0.01) => 0.2499875
; (simpson-integral cube 0.0 1.0 100) => 0.25
; (integral cube 0 1 0.001) => 0.249999875000001
; (simpson-integral cube 0.0 1.0 1000) => 0.25

; Exercise 1.30
(define (sum-iter term a next b)
  "Compute the sum of a series iteratively.

TERM is the expression to sum.

A is the lower bound.

NEXT is the expression used to determine the next input.

B is the final integer in the series."
  (define (iter a result)
    "Helper function for `sum-iter'."
    (if (> a b)
        result
        (iter (next a) (+ (term a) result))))
  (iter a 0))

; Exercise 1.31
; a.)
(define (product term a next b)
  "Compute the product of a series.

TERM is the expression to multiply.

A is the lower bound.

NEXT is the expression used to determine the next input.

B is the upper bound."
  (if (> a b)
      1
      (* (term a)
         (product term (next a) next b))))

(define (factorial n)
  "Compute the factorial of integer N."
  (product identity 1 inc n))

(define (pi-approx n)
  "Approximate pi to N places using Wallis' formula."
  (define (term a)
    "Term function for `pi-approx'."
    (/ (* a (+ a 2.0)) (* (+ a 1.0) (+ a 1.0))))
  (define (next a)
    "Next functionfor `pi-approx'."
    (+ a 2.0))
  (* 4.0 (product term 2.0 next (+ 2 (* 2 n)))))

; b.)
(define (product-iter term a next b)
  "Compute the product of a series iteratively.

TERM is the expression to multiply.

A is the lower bound.

NEXT is the expression used to determine the next in the series.

B is the upper bound."
  (define (iter a result)
    "Helper function for `product-iter'."
    (if (> a b)
        result
        (iter (next a) (* (term a) result))))
  (iter a 1))

; Exercise 1.32
; a.)
(define (accumulate combiner null-value term a next b)
  "Accumulate a series into a result.

COMBINER is the expression used to combine the elements of the series.

NULL-VALUE is the value to return when A > B.

TERM is the expression to combine.

A is the lower bound.

NEXT is the expression used to compute the next element in the series.

B is the upper bound."
  (if (> a b)
      null-value
      (combiner (term a)
                (accumulate combiner null-value term (next a) next b))))

(define (sum-accumulate term a next b)
  "Compute the sum of a series using `accumulate'.

TERM is the expression to sum.

A is the lower bound.

NEXT is the expression used to determine the next input.

B is the final integer in the series."
  (accumulate + 0 term a next b))

(define (product-accumulate term a next b)
  "Compute the product of a series using `accumulate'.

TERM is the expression to multiply.

A is the lower bound.

NEXT is the expression used to determine the next in the series.

B is the upper bound."
  (accumulate * 1 term a next b))

; b.)
(define (accumulate-iter combiner null-value term a next b)
  "Accumultate a series iteratively.

COMBINER is the expression used to combine the elements of the series.

NULL-VALUE is the value to initialize the result.

TERM is the expression to multiply.

A is the lower bound.

NEXT is the expression used to determine the next in the series.

B is the upper bound."
  (define (iter x result)
    "Helper function for `accumulate-iter'."
    (if (> x b)
        result
        (iter (next x) (combiner (term x) result))))
  (iter a null-value))

; Exercise 1.33
(define (accumulate-filtered filter combiner null-value term a next b)
  "Accumultate a filtered-series iteratively.

FILTER is the expression used to filter the series.

COMBINER is the expression used to combine the elements of the series.

NULL-VALUE is the value to initialize the result.

TERM is the expression to multiply.

A is the lower bound.

NEXT is the expression used to determine the next in the series.

B is the upper bound."
  (define (iter x result)
    "Helper function for `accumulate-iter'."
    (if (> x b)
        result
        (iter (next x)
              (if (filter x)
                  (combiner (term x) result)
                  result))))
  (iter a null-value))

; a.)
(define (sum-prime-squares a b)
  "Compute the sum of the squares of all prime numbers between A and B inclusive."
  (accumulate-filtered prime? + 0 square a inc b))

; b.)
(define (product-relatively-prime n)
  "Compute the product of all positive integers less than N that are relatively prime to N."
  (accumulate-filtered fermat-test * 1 identity 1 inc (- n 1)))

; Section 1.3.2

(define (pi-sum a b)
  "Compute the sum of the series 1/(a*(a+2)) + 1/((a+2)*(a+4)) from A to B inclusive using `sum' and `lambda'."
  (sum (lambda (x) (/ 1.0 (* x (+ x 2))))
       a
       (lambda (x) (+ x 4))
       b))

(define (integral f a b dx)
  "Approximate integral of function using `lambda'.

F is the function.

A is the lower bound.

B is the upper bound.

DX is the change in the input."
  (* (sum f
          (+ a (/ dx 2.0))
          (lambda (x) (+ x dx))
          b)
     dx))

(define (f x y)
  "Compute function using helper function."
  (define (f-helper a b)
    (+ (* x (square a))
       (* y b)
       (* a b)))
  (f-helper (+ 1 (* x y)) 
            (- 1 y)))

(define (f x y)
  "Compute function using `lambda'."
  ((lambda (a b)
     (+ (* x (square a))
        (* y b)
        (* a b)))
   (+ 1 (* x y))
   (- 1 y)))

(define (f x y)
  "Compute function using `let'."
  (let ((a (+ 1 (* x y)))
        (b (- 1 y)))
    (+ (* x (square a))
       (* y b)
       (* a b))))

(define (f x y)
  "Compute function using internal defines."
  (define a (+ 1 (* x y)))
  (define b (- 1 y))
  (+ (* x (square a))
     (* y b)
     (* a b)))

; Exercise 1.34
(define (f g)
  "Call function G with value 2."
  (g 2))
; (f f) => (f 2) => (2 2)

; Section 1.3.3

(define (average x y)
  "Compute the average of numbers X and Y."
  (/ (+ x y) 2))

(define (close-enough? x y)
  "Determine if X and Y are close-enough together."
  (< (abs (- x y)) 0.001))

(define (search f neg-point pos-point)
  "Search for root of function.

F is the function to search.

neg-point is a point where F(neg-point) < 0.

pos-point is a point where F(pos-point) > 0."
  (let ((midpoint (average neg-point pos-point)))
    (if (close-enough? neg-point pos-point)
        midpoint
        (let ((test-value (f midpoint)))
          (cond ((positive? test-value)
                 (search f neg-point midpoint))
                ((negative? test-value)
                 (search f midpoint pos-point))
                (else midpoint))))))

(define (half-interval-method f a b)
  "Find a root of a function using the half-interval method.

F is the function to search.

A is the lower bound.

B is the upper bound."
  (let ((a-value (f a))
        (b-value (f b)))
    (cond ((and (negative? a-value) (positive? b-value))
           (search f a b))
          ((and (negative? b-value) (positive? a-value))
           (search f b a))
          (else (error "Values are not of opposite sign" a b)))))

(define tolerance 0.00001)
(define (fixed-point f first-guess)
  "Compute fixed-point of function.

F is the function.

FIRST-GUESS is the first guess to the result."
  (define (close-enough? v1 v2)
    "Determine if the difference between V1 and V2 is below the tolerance."
    (< (abs (- v1 v2)) tolerance))
  (define (try guess)
    "Try a guess."
    (let ((next (f guess)))
      (if (close-enough? guess next)
          next
          (try next))))
  (try first-guess))

(define (sqrt x)
  "Compute square root of X using `fixed-point'."
  (fixed-point (lambda (y) (/ x y))
               1.0))

(define (sqrt x)
  "Compute square root of X using `fixed-point' and `average'."
  (fixed-point (lambda (y) (average y (/ x y)))
               1.0))

; Exercise 1.35
; phi^2 = phi + 1 => phi = 1 + 1/phi = f(phi)
; Therefore, f(x) = x, so the golden ratio is a fixed point
(define phi
  (fixed-point (lambda (x) (+ 1 (/ 1 x))) 1.0))

; Exercise 1.36
(define (fixed-point-verbose f first-guess)
  "Compute fixed-point of function.

F is the function.

FIRST-GUESS is the first guess to the result."
  (define (close-enough? v1 v2)
    "Determine if the difference between V1 and V2 is below the tolerance."
    (< (abs (- v1 v2)) tolerance))
  (define (try guess)
    "Try a guess."
    (let ((next (f guess)))
      (display next)
      (newline)
      (if (close-enough? guess next)
          next
          (try next))))
  (display first-guess)
  (newline)
  (try first-guess))
; (fixed-point-verbose (lambda (x) (/ (log 1000) (log x))) 2.0)
; 4.55553227080365 in 36 steps
; (fixed-point-verbose (lambda (x) (average x (/ (log 1000) (log x)))) 2.0)
; 4.55553755199982 in 10 steps

; Exercise 1.37
; a.)
(define (cont-frac n d k)
  "Compute the continued fraction for a certain number of steps.

N is the expression used to compute the numerator.

D is the expression used to compute the denominator.

K is the number of steps to compute."
  (define (cont-frac-helper i)
    "Helper function for `cont-frac'."
    (/ (n i)
       (+ (d i)
          (if (< i k)
              (cont-frac-helper (inc i))
              0))))
  (cont-frac-helper 0))
; (cont-frac (lambda (i) 1.0) (lambda (i) 1.0) 10) => 0.6180555555...

; b.)
(define (cont-frac-iter n d k)
  "Compute the continued fraction iteratively for a certain number of steps.

N is the expression used to compute the numerator.

D is the expression used to compute the denominator.

K is the number of steps to compute."
  (define (cont-frac-helper i result)
    "Helper function for `cont-frac'."
    (if (= i 0)
        result
        (cont-frac-helper (- i 1)
                          (/ (n i)
                             (+ (d i) result)))))
  (cont-frac-helper k (/ (n (- k 1)) (d (- k 1)))))

; Exercise 1.38
(define (e-approximate k)
  "Approximate e using `cont-frac' for K steps."
  (let ((n (lambda (i) 1.0))
        (d (lambda (i) (let ((rem (remainder i 3)))
                         (cond ((= rem 0) 1.0)
                               ((= rem 1) (+ 2.0 (/ i 3.0)))
                               ((= rem 2) 1.0))))))
    (+ 2.0 (cont-frac n d k))))

; Exercise 1.39
(define (tan-cf x k)
  "Compute tangent of X via K steps of Lambert's formula."
  (* -1.0 (cont-frac (lambda (i) (- (expt x (inc i))))
                     (lambda (i) (+ (* 2.0 i) 1.0))
                     k)))

; Section 1.3.4

(define (average-damp f)
  "Compute average of x and F(x)."
  (lambda (x) (average x (f x))))

(define (sqrt x)
  "Compute square root of X using `fixed-point' and `average-damp'"
  (fixed-point (average-damp (lambda (y) (/ x y)))
               1.0))

(define (cube-root x)
  "Compute cube root of X using `fixed-point' and `average-damp'"
  (fixed-point (average-damp (lambda (y) (/ x (square y))))
               1.0))

(define (deriv g)
  "Compute the derivative of function G."
  (lambda (x)
    (/ (- (g (+ x dx))
          (g x))
       dx)))

(define (newton-transform g)
    "Transform method G."
    (lambda (x)
      (- x (/ (g x) ((deriv g) x)))))

(define (newtons-method g guess)
  "Find an approximate solution to an equation.

G is the expression containing the equation.

GUESS is a potential solution."
  
  (fixed-point (newton-transform g) guess))

(define (sqrt x)
  "Compute the square root of X using `newtons-method'."
  (newtons-method (lambda (y) (- (square y) x))
                  1.0))

(define (fixed-point-of-transform g transform guess)
  "Compute fixed point of transform."
  (fixed-point (transform g) guess))

(define (sqrt x)
  "Compute square root of X using `fixed-point-of-transform'."
  (fixed-point-of-transform (lambda (y) (- (square y) x))
                            newton-transform
                            1.0))

; Exercise 1.40
(define (cubic a b c)
  "Return expression to compute x^3 + Ax^2 + Bx + C."
  (lambda (x)
    (+ (cube x) (* a (square x)) (* b x) c)))

; Exercise 1.41
(define (double g)
  "Return an expression that applies expression G twice."
  (lambda (x)
    (g (g x))))
; (((double (double double)) inc) 5) => 21

; Exercise 1.42
(define (compose f g)
  "Return composition of expressions F & G."
  (lambda (x)
    (f (g x))))

; Exercise 1.43
(define (repeated f i)
  "Return expression to compute the Ith iteration of f(f(f(...(f(x)))))."
  (if (= i 1)
      f
      (compose f (repeated f (- i 1)))))

; Exercise 1.44

(define (smooth f dx)
  "Smooth a function.

F is the function.

DX is the change in input values."
  (lambda (x)
    (/ (+ (f (- x dx))
          (f x)
          (f (+ x dx)))
       3)))

(define (n-fold-smooth f dx i)
  "Compute an n-fold smoothed function.

F is the function.

I is the number of times to apply the smoothing operation.

DX is the change in input value to the function."
  (repeated (smooth f dx) i))

; Exercise 1.45
(define (nth-root nth x)
  "Compute the NTH root of number X."
  (fixed-point 
   ((repeated average-damp (/ (log nth) (log 2))) 
    (lambda (y) 
      (/ x (expt y (- nth 1)))))
   1.0))

; Exercise 1.46
(define (iterative-improve good-enough? improve)
  "Return a function that keeps calling the IMPROVE function until the GOOD-ENOUGH? function returns true.

GOOD-ENOUGH? is a function that takes two arguments:
  the current guess
  the improved guess

IMPROVE is a function that takes one argument, the current guess."
  (lambda (first-guess)
    (define (try guess)
      (let ((next (improve guess)))
        (if (close-enough? guess next)
            next
            (try next))))
    (try first-guess)))

(define (iterative-fixed-point f first-guess)
  "Compute fixed-point of function using `iterative-improve'.

F is the function.

FIRST-GUESS is the first guess to the result."
  ((iterative-improve (lambda (x y) 
                       (< (abs (- x y)) 0.00001))
                     f)
   first-guess))

(define (iterative-sqrt x)
  "Compute the square root of number X using `iterative-improve'."
  (iterative-fixed-point (lambda (y)
                           (/ x y))
                         1.0))
