; Section 2.1.1

(define (make-rat n d)
  "Create rational number with numerator N and denominator D."
  (let ((g (gcd n d)))
    (cons (/ n g) (/ d g))))

(define (numer r)
  "Extract numerator from rational number R."
  (car r))

(define (denom r)
  "Extract denominator from rational number R."
  (cdr r))

(define (add-rat x y)
  "Add rational numbers X and Y."
  (make-rat (+ (* (numer x) (denom y))
               (* (numer y) (denom x)))
            (* (denom x) (denom y))))

(define (sub-rat x y)
  "Subtract rational numbers X and Y."
  (make-rat (- (* (numer x) (denom y))
               (* (numer y) (denom x)))
            (* (denom x) (denom y))))

(define (mul-rat x y)
  "Multiply rational numbers X and Y."
  (make-rat (* (numer x) (numer y))
            (* (denom x) (denom y))))

(define (div-rat x y)
  "Divide rational numbers X and Y."
  (make-rat (* (numer x) (denom y))
            (* (denom x) (numer y))))

(define (equal-rat? x y)
  "Test rational numbers X and Y for equality."
  (= (* (numer x) (denom y))
     (* (numer y) (denom x))))

(define (print-rat x)
  "Print rational number X."
  (newline)
  (display (numer x))
  (display "/")
  (display (denom x))
  (newline))

; Exercise 2.1
(define (sign-make-rat n d)
  "Create rational number from signed numerator N and signed denominator D."
  (let ((sign (if (or (and (>= n 0) (> d 0))
                      (and (< n 0) (< d 0)))
                  1
                  -1)))
    (cons (* sign (abs n)) (abs d))))

; Section 2.1.2

; Exercise 2.2

(define (make-point x y)
  "Create a point with coordinates X and Y."
  (cons x y))

(define (x-point p)
  "Extract x-coordinate from point P."
  (car p))

(define (y-point p)
  "Extract y-coordinate from point P."
  (cdr p))

(define (make-segment start-point end-point)
  "Create a line segment beginning at START-POINT and ending at END-POINT."
  (cons start-point end-point))

(define (start-segment l)
  "Extract start point from line L."
  (car l))

(define (end-segment l)
  "Extract end point from line L."
  (cdr l))

(define (midpoint-segment l)
  "Determine midpoint of line L."
  (make-point (average (x-point (start-segment l))
                       (x-point (end-segment l)))
              (average (y-point (start-segment l))
                       (y-point (end-segment l))))) 

(define (print-point p)
  "Print point P."
  (newline)
  (display "(")
  (display (x-point p))
  (display ",")
  (display (y-point p))
  (display ")")
  (newline))

; Exercise 2.3
(define (make-rectangle a b c d)
  "Create a rectangle from points A B C and D."
  (cons a (cons b (cons c d))))

(define (length r)
  "Determine length of rectangle R."
  (abs (- (y-point (cdr (cdr (cdr r))))
          (y-point (car (cdr r))))))

(define (width r)
  "Determine the width of rectangle R."
  (abs (- (x-point (car (cdr r)))
          (x-point (car r)))))

(define (perimeter r)
  "Compute the perimeter of rectangle R."
  (* 2 (+ (length r) (width r))))

(define (area r)
  "Compute the area of rectangle R."
  (* (length r) (width r)))

(define (other-make-rectangle s t)
  "Create a rectangle from segments S and T."
  (cons s t))

(define (other-length r)
  "Determine length of rectangle R."
  (let ((x1 (x-point (start-segment (cdr r))))
        (x2 (x-point (end-segment (cdr r))))
        (y1 (y-point (start-segment (cdr r))))
        (y2 (y-point (end-segment (cdr r)))))
    (sqrt (+ (square (- x2 x1))
             (square (- y2 y1))))))

(define (other-width r)
  "Determine width of rectangle R."
  (let ((x1 (x-point (start-segment (car r))))
        (x2 (x-point (end-segment (car r))))
        (y1 (y-point (start-segment (car r))))
        (y2 (y-point (end-segment (car r)))))
    (sqrt (+ (square (- x2 x1))
             (square (- y2 y1))))))

(define (perimeter length width r)
  "Compute the pereimeter of a rectangle.

LENGTH is the expression to determine the length of the rectangle.

WIDTH is the expression to determine the width of the rectangle.

R is the rectangle."
  (* 2 (+ (length r) (width r))))

(define (area length width r)
  "Compute the area of a rectangle.

LENGTH is the expression to determine the length of the rectangle.

WIDTH is the expression to determine the width of the rectangle.

R is the rectangle."
  (* (length r) (width r)))

; Section 2.1.3

(define (cons x y)
  "Construct a cons cell with head X and tail Y."
  (define (dispatch m)
    "Helper function for `cons'."
    (cond ((= m 0) x)
          ((= m 1) y)
          (else (error "Argument not 0 or 1 -- CONS" m))))
  dispatch)

(define (car z)
  "Return head element of cons cell Z."
  (z 0))

(define (cdr z)
  "Return tail element of cons cell Z."
  (z 1))

; Exercise 2.4
(define (cons x y)
  "Construct a cons cell with head X and tail Y."
  (lambda (m) (m x y)))

(define (car z)
  "Return head element of cons cell Z."
  (z (lambda (p q) p)))

; (car (cons x y)) => ((lambda (m) (m x y)) (lambda (p q) p))
; ((lambda (p q) p) x y) => x

(define (cdr z)
  "Return tail element of cons cell Z."
  (z (lambda (p q) q)))
; (cdr (cons x y)) => ((lambda (m) (m x y)) (lambda (p q) q))
; ((lambda (p q) q) x y) => y

; Exercise 2.5
(define (cons a b)
  "Represent the pair A and B as 2^A*3^B."
  (* (expt 2 a) (expt 3 b)))

(define (car x)
  "Return head element of cons cell Z."
  (if (= (remainder x 3) 0)
      (car (/ x 3))
      (/ (log x) (log 2))))

(define (cdr x)
  "Return tail element of cons cell Z."
  (if (= (remainder x 2) 0)
      (cdr (/ x 2))
      (/ (log x) (log 3))))

; Exercise 2.6
(define zero (lambda (f) (lambda (x) x)))

(define (add-1 n)
  (lambda (f) (lambda (x) (f ((n f) x)))))

; (add-1 zero) =>
; (lambda (f)
;   (lambda (x)
;     (f ((zero f) x)))) =>
; (lambda (f)
;   (lambda (x)
;     (f (((lambda (f2)
;            (lambda (x2)
;              x2))
;          f)
;         x)))) =>
; (lambda (f)
;   (lambda (x)
;     (f (((lambda (x2)
;            x2))
;         x)))) =>
; (lambda (f)
;   (lambda (x)
;     (f x)))

(define (one)
  "Implement number 1 using Church numerals."
  (lambda (f)
   (lambda (x)
     (f x))))

; (add-1 one) =>
; (lambda (f)
;  (lambda (x)
;    (f ((one f) x)))) =>
; (lambda (f)
;   (lambda (x)
;     (f (((lambda (f2)
;            (lambda (x2)
;              (f2 x2))) f) x)))) =>
; (lambda (f)
;   (lambda (x)
;     (f (((lambda (x2)
;              (f x2)))
;         x)))) =>
; (lambda (f)
;   (lambda (x)
;     (f (f x))))

(define (two)
  "Implement number 2 using Church numerals."
  (lambda (f)
    (lambda (x)
      (f (f x)))))

(define (add a b)
  "Add Church numerals A and B."
  (lambda (f)
    (lambda (x)
      ((a f) ((b f) x)))))
