(define pi 3.14159)
(define radius 10)
(define circumference (* 2 pi radius))
(define (square x) (* x x))
(define (sum-of-squares x y)
  (+ (square x) (square y)))
(define (f a)
  (sum-of-squares (+ a 1) (* a 2)))
(define (abs x)
  (if (< x 0)
      (- x)
      x))
(define (>= x y)
  (not (< x y)))

; Exercise 1.2
;(/
; (+ 5 4 (- 2 (- 3 (+ 6 (/ 4 5)))))
; (* 3
;    (- 6 2)
;    (- 2 7)))

; Exercise 1.3
(define (larger-sum-of-squares x y z)
  (sum-of-squares
   (if (>= x y) x y)
   (if (>= y z) y z)))

; Exercise 1.4
;If b > 0, b is added to a. Otherwise, b is subtracted from a.

; Exercise 1.5
; Applicative order
;(test 0 (p))
;(if (= 0 0) 0 (p))
;0
; Normal order
;(test 0 (p))
;(if (= 0 0) 0 (p))
;(if (= 0 0) 0 (p))
; infinite loop

; Section 1.1.7-1.1.8
(define (sqrt x)
  (define (average num1 num2)
    (/ (+ num1 num2) 2))
  (define (good-enough? guess)
    (< (abs (- (square guess) x)) 0.001))
  (define (improve guess)
    (average guess (/ x guess)))
  (define (sqrt-iter guess)
    (if (good-enough? guess)
        guess
        (sqrt-iter (improve guess))))
  (sqrt-iter 1.0))

; Exercise 1.6
;(define (new-if predicate then-clause else-clause)
;  (cond (predicate then-clause)
;        (else else-clause)))
;(define (sqrt-iter guess x)
;  (new-if (good-enough? guess x)
;          guess
;          (sqrt-iter (improve guess x)
;                     x)))

; Exercise 1.7
(define (good-enough? guess x)
  (<
   (abs (/ (- (improve guess x) guess) (improve guess x)))
   0.001))

; Exercise 1.8
(define (cube x)
  (* x x x))
(define (cbrt-improve guess x)
  (/ (+ (/ x (cube guess)) (* guess 2)) 3))
(define (cbrt-good-enough? guess x)
  (<
   (abs (/ (- (cbrt-improve guess x) guess) (improve guess x)))
   0.001))
(define (cbrt-iter guess x)
  (if (cbrt-good-enough? guess x)
      guess
      (cbrt-iter (cbrt-improve guess x)
                 x)))
(define (cbrt x)
  (cbrt-iter 1.0 x))
