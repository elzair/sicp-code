(load "section1-1.scm")

; 1.2.1

(define (factorial-recursive n)
  "Compute N! as a recursive process."
  (if (= n 1)
      1
      (* n (factorial (- n 1)))))
(define (factorial n)
  "Compute N! as an interative process."
  (define (fact-iter product counter)
    (if (> counter n)
        product
        (fact-iter (* counter product)
                   (+ counter 1))))
  (fact-iter 1 1))

; Exercise 1.9
; First +
; (+ 5 3)
; (if (= 5 0) 3 (inc (+ (dec 5) 3)))
; (inc (+ 4 3))
; (inc (if (= 4 0) 3 (inc (+ (dec 4) 3))) 3)
; (inc (inc (+ 3 3)) 3)
; This function is recursive
; Second +
; (+ 5 3)
; (if (= 5 0) 3 (+ (dec 5) (inc 3)))
; (+ 4 4)
; This function is iterative 

; Exercise 1.10
(define (A x y)
  "Compute Ackermann function for inputs X and Y."
  (cond ((= y 0) 0)
        ((= x 0) (* 2 y))
        ((= y 1) 2)
        (else (A (- x 1)
                 (A x (- y 1))))))
; (= (A 1 0) 1024)
; (= (A 2 4) 65536)
; (= (A 3 3) 65536)
(define (f n) (A 0 n)) ; = (* 2 n)
(define (g n) (A 1 n)) ; = (^ 2 n)
(define (h n) (A 2 n)) ; = (^ 2 (h (- n 1)))

;(define (^ x y)
;  (define (^-iter counter product)
;    (if (> counter y)
;        product
;        (^-iter (+ counter 1) (* product x))))
;  (^-iter 1 1))
(define (^ x y)
  (define (^-iter counter product)
    (if ((if (< y 0) < >) counter y)
        product
        (^-iter ((if (< y 0) - +) counter 1) ((if (< y 0) / *) product x))))
  (^-iter (if (< y 0) (- 1) 1) 1))

; 1.2.2

(define (fib-rec n)
  "Evaluate fibonacci number of position N as a recursive process."
  (cond ((= n 0) 0)
        ((= n 1) 1)
        (else (+ (fib-rec (- n 1))
                 (fib-rec (- n 2))))))

(define (fib n)
  "Compute Nth fibonacci number as an iterative process."
  (define (fib-iter a b count)
    "Helper method for `fib'.

A is the previous fibonacci number.

B is the fibonacci number previous to A.

COUNT is the current position of the evaluation."
    (if (= count 0)
        b
        (fib-iter (+ a b) a (- count 1))))
  (fib-iter 1 0 n))

(define (count-change amount)
  "Count the different ways to make change of AMOUNT."
  (define (cc amt kinds-of-coins)
    "Helper function for `count-change'.

AMT is the amount.

KINDS-OF-COINS is the number of different types of coins available."
    (define (first-denomination)
      "Determine the first-denomination for the number of coins available."
      (cond ((= kinds-of-coins 1) 1)
            ((= kinds-of-coins 2) 5)
            ((= kinds-of-coins 3) 10)
            ((= kinds-of-coins 4) 25)
            ((= kinds-of-coins 5) 50)))
    (cond ((= amt 0) 1)
          ((or (< amt 0) (= kinds-of-coins 0)) 0)
          (else (+ (cc amt
                       (- kinds-of-coins 1))
                   (cc (- amt
                          (first-denomination))
                       kinds-of-coins)))))
  (cc amount 5))

; Exercise 1.11
(define (f-rec n)
  "Compute f(n) by a recursive-process."
  (cond ((< n 3) n)
        (else (+ (f-rec (- n 1))
                 (* 2 (f-rec (- n 2)))
                 (* 3 (f-rec (- n 3)))))))
; (f 0) -> 0
; (f 1) -> 1
; (f 2) -> 2
; (f 3) -> (+ 2 (* 2 1) (* 3 0)) -> 4
; (f 4) -> (+ 4 (* 2 2) (* 3 1)) -> 11
; (f 5) -> (+ 11 (* 2 4) (* 3 2)) -> 25

(define (f-iter n)
  "Compute f(n) = f(n-1) + 2f(n-2) + 3f(n-3) by an iterative process."
  (define (f-iter-help a b c count)
    "Helper function for `f-iter'"
    (cond ((<= count 0) c)
          (else (f-iter-help b
                             c
                             (+ c
                                (* 2 b)
                                (* 3 a))
                             (- count 1)))))
  (cond ((< n 3) n)
        (else (f-iter-help 0 1 2 (- n 2)))))

; Exercise 1.12
(define (pascal row column)
  "Compute pascal element at ROW COLUMN."
  (cond  ((or (< row 0) (> column row)) (error "Invalid ROW and/or COLUMN"))
         ((or (= column 0) (= column row)) 1)
         (else (+ (pascal (- row 1) (- column 1))
                  (pascal (- row 1) column)))))

; Exercise 1.13
; phi^2 = phi + 1 -> phi = 1 + 1/phi
; Let gamma = (1 - (sqrt(5))/2)
; gamma^2 = gamma + 1 -> gamma = 1 + 1/gamma
; Prove fib(n) = (phi^2 - gamma^2)/sqrt(5)
; n=0: 0 = (phi^0 - gamma^0)/sqrt(5) = (1 - 1)/sqrt(5) = 0
; n=1: 1 = (phi - gamma)/sqrt(5) = (1+sqrt(5)-1+sqrt(5))/(2*sqrt(5)) = 1
; Assume Fib(n) = (phi^n - gamma^n)/sqrt(5)
; and fib(n-1) = (phi^(n-1) - gamma^(n-1))/sqrt(5)
; fib(n+1) = fib(n)+fib(n-1)
; = (phi^n - gamma^n + phi^(n-1) - gamma^(n-1)) / sqrt(5)
; = (phi^n*(1 + 1/phi) - gamma^2*(1 + 1/gamma)) / sqrt(5)
; = (phi^(n+1) - gamma^(n+1)) / sqrt(5)
; Therefore, by induction, fib(n) = (phi^n - gamma^n) / sqrt(5)

; 1.2.3

; Exercise 1.14
(define num-iters 0)
(define (first-denomination-test kinds-of-coins)
  "Determine the first-denomination for the number of coins available."
  (cond ((= kinds-of-coins 1) 1)
        ((= kinds-of-coins 2) 5)
        ((= kinds-of-coins 3) 10)
        ((= kinds-of-coins 4) 25)
        ((= kinds-of-coins 5) 50)))
(define (count-change-test amount)
  "Count the different ways to make change of AMOUNT."
  (set! num-iters 0)
  (cc-test amount 5))
(define (cc-test amt kinds-of-coins)
  "Helper function for `count-change'.

AMT is the amount.

KINDS-OF-COINS is the number of different types of coins available."
  (set! num-iters (1+ num-iters))
  (cond ((= amt 0) 1)
        ((or (< amt 0) (= kinds-of-coins 0)) 0)
        (else
         (+ (cc-test amt
                (- kinds-of-coins 1))
            (cc-test (- amt
                   (first-denomination-test kinds-of-coins))
                kinds-of-coins))
         num-iters)))

; Exercise 1.15
(define (cube x)
  "Return X^3."
  (* x x x))
(define (p x)
  "Test function for X."
  (- (* 3 x) (* 4 (cube x))))
(define (sine angle)
  "Return sin(ANGLE) in radians."
  (if (not (> (abs angle) 0.1))
      angle
      (p (sine (/ angle 3.0)))))
; a.)
; (sine 12.15)
; (p (sine 4.05))
; (p (p (sine 1.34999999)))
; (p (p (p (sine 0.44999999999996))))
; (p (p (p (p (sine 0.15)))))
; (p (p (p (p (p (sine 0.0499999999996))))))
; 5
; b.) Since the space is directly proportional its increase is O(a).
;     the number of steps is O(log(n)).

; 1.2.4

(define (expt-rec b n)
"Compute B^N by a recursive process."
(if (= n 0)
    1
    (* b (expt b (- n 1)))))

(define (expt-iter b n)
  "Compute B^N by an interative process."
  (define (expt-helper base counter product)
    "Helper function for `expt-iter'."
    (if (= counter 0)
        product
        (expt-helper base 
                   (- counter 1)
                   (* base product))))
  (expt-helper b n 1))

(define (even? n)
  "Determine if number N is even or not."
  (= (remainder n 2) 0))

(define (fast-expt b n)
  "Compute B^N using a recursive successive squaring method."
  (cond ((= n 0) 1)
        ((even? n) (square (fast-expt b (/ n 2))))
        (else (* b (fast-expt b (- n 1))))))

; Exercise 1.16
; 1*3^5 = (1*3)*3^4 = (1*3)*(3^2)^4/2 = (1*3*3^2)*(3^2)^4/2-1 81*3^1 = 243*3^0 = 243
; 1*3^4 = 3*3^3 = 9*3^2 = 
(define (faster-expt base exp)
  "Compute BASE^EXP using an iterative successive squaring method."
  (define (faster-expt-helper a b n)
    "Helper function for `faster-expt'"
    (cond ((= n 0) a)
          ((even? n) (faster-expt-helper (* a (square b))
                                         (square b)
                                         (- (/ n 2) 1)))
          (else (faster-expt-helper (* a b) b (- n 1)))))
  (faster-expt-helper 1 base exp))

; Exercise 1.17
(define (*-ex a b)
  "Multiplies A and B."
  (if (= b 0)
      0
      (+ a (*-ex a (- b 1)))))
(define (double x)
  "Doubles integer X."
  (+ x x))
(define (halve x)
  "Halves integer X."
  (/ x 2))

(define (*-fast a b)
  "Multiplies integers A and B by a successive doubling method."
  (cond ((= b 0) 0)
        ((even? b) (double (*-fast a (halve b))))
        (else (+ a (*-fast a (- b 1))))))

; Exercise 1.18
(define (*-faster a b)
  "Compute A*B using an iterative successive doubling method."
  (define (*-faster-helper c x y)
    "Helper function for `faster-expt'"
    (cond ((= y 0) c)
          ((even? y) (*-faster-helper (+ c (double x))
                                      (double x)
                                      (- (halve y) 1)))
          (else (*-faster-helper (+ c x) x (- y 1)))))
  (*-faster-helper 0 a b))

; Exercise 1.19
; Let T_pq be a translation of (a, b) s.t.
; T_pq(a, b) => (bq+aq+ap, bp+aq) e R
; Let p = 0 and q = 1
; T_pq(a,b) => T_01(a,b) => b*1 + a*1 + a*0, b*0 + a*1 => a + b, a
; Now, Let (a_1, b_1) => T_pq(a,b) => (bq+aq+ap, bp+aq)
; Therefore, (a_2, b_2) => T_pq(a_1, b_1) => (b_1q+a_1q+a_1p, b_1p+a_1q) =>
;   ((bp+aq)q + (bq+aq+ap)q + (bq+aq+ap)p, (bp+aq)p + (bq+aq+ap)q)    
; Therefore, b2 = bpp + aqp + bqq + aqq + apq = b(p^2+q^2) + a(2pq+q^2) 
; Therefore, p' = p^2 + q^2, q' = 2pq + q^2
(define (fib n)
  "Compute the fibonacci number for position N."
  (define (fib-iter a b p q count)
    "Helper method for `fib'"
    (cond ((= count 0) b)
          ((even? count)
           (fib-iter a
                     b
                     (+ (square p) (square q))
                     (+ (* 2 p q) (square q))
                     (/ count 2)))
          (else (fib-iter (+ (* b q) (* a q) (* a p))
                          (+ (* b p) (* a q))
                          p
                          q
                          (- count 1)))))
  (fib-iter 1 0 0 1 n))

; 1.2.5

(define (gcd a b)
  "Compute the Greatest Common Divisor of numbers A and B."
  (if (= b 0)
      a
      (gcd b (remainder a b))))

; Exercise 1.20
; Normal Order Evaluation
; (gcd 206 40)
; (if (= 40 0)
;   206
;   (gcd 40 
;   (remainder 206 40)))
; (gcd 40 
;      (remainder 206 40))
; (if (= (remainder 206 40) 0) 
;   40 
;   (gcd (remainder 206 40) 
;        (remainder 40 (remainder 206 40))))
; 1 remainder evaluated
; (gcd (remainder 206 40) 
;      (remainder 40 (remainder 206 40)))
; (if (= (remainder 40 (remainder 206 40)) 0)
;  (remainder 206 40)
;  (gcd (remainder 40 (remainder 206 40)) 
;       (remainder (remainder 206 40) (remainder 40 (remainder 206 40)))))
; 3 remainders evaluated
; (gcd (remainder 40 (remainder 206 40)) (remainder (remainder 206 40) (remainder 40 (remainder 206 40)))
; (if (= (remainder (remainder 206 40) (remainder 40 (remainder 206 40))) 0)
;   (remainder 40 (remainder 206 40))
;   (gcd (remainder (remainder 206 40) (remainder 40 (remainder 206 40))) 
;        (remainder (remainder 40 (remainder 206 40)) (remainder (remainder 206 40) (remainder 40 (remainder 206 40))))))
; 7 remainders evaluated
; (gcd (remainder (remainder 206 40) (remainder 40 (remainder 206 40))) 
;      (remainder (remainder 40 (remainder 206 40)) (remainder (remainder 206 40) (remainder 40 (remainder 206 40))))))
; (if (= (remainder (remainder 40 (remainder 206 40)) (remainder (remainder 206 40) (remainder 40 (remainder 206 40)))) 0)
;   (remainder (remainder 206 40) (remainder 40 (remainder 206 40)))
;   (gcd (remainder (remainder 40 (remainder 206 40)) (remainder (remainder 206 40) (remainder 40 (remainder 206 40))))
;        (remainder (remainder (remainder 206 40) (remainder 40 (remainder 206 40))) 
;                   (remainder (remainder 40 (remainder 206 40)) (remainder (remainder 206 40) (remainder 40 (remainder 206 40))))))
; 14 remainders evaluated
; (remainder (remainder 206 40) (remainder 40 (remainder 206 40)))
; 18 remainders evaluated in total
; Applicative Order Evaluation
; (gcd 206 40)
; (if (= 40 0) 206 (gcd 40 (remainder 206 40)))
; 1 remainder evaluated
; (if (= 40 0) 206 (gcd 40 6))
; (gcd 40 6)
; (if (= 6 0) 40 (gcd 6 (remainder 40 6)))
; 2 remainders evaluated
; (if (= 6 0) 40 (gcd 6 4))
; (gcd 6 4)
; (if (= 4 0) 6 (gcd 4 (remainder 6 4)))
; 3 remainders evaluated
; (if (= 4 0) 6 (gcd 4 2))
; (gcd 4 2)
; (if (= 2 0) 4 (gcd 2 (remainder 4 2)))
; 4 remainders evaluated
; (if (= 2 0) 4 (gcd 2 0))
; (gcd 2 0)
; (if (= 0 0) 2 (gcd (remainder 2 0)))
; 2
; 4 remainders evaluated in total

; 1.2.6

(define (divides? a b)
  "Determine if number A divides number B."
  (= (remainder b a) 0))

(define (find-divisor n test-divisor)
  "Find the divisor of number N."
  (cond ((> (square test-divisor) n) n)
        ((divides? test-divisor n) test-divisor)
        (else (find-divisor n (+ test-divisor 1)))))

(define (smallest-divisor n)
  "Find the smallest divisor of number N."
  (find-divisor n 2))

(define (prime? n)
  "Determine if number N is prime."
  (= n (smallest-divisor n)))

; Fermat test
(define (expmod base exp m)
  "Compute exponential of BASE to the power EXP modulo M."
  (cond ((= exp 0) 1)
        ((even? exp)
         (remainder (square (expmod base (/ exp 2) m))
                    m))
        (else (remainder (* base (expmod base (- exp 1) m))
                         m))))

(define (fermat-test n)
  "Test if number N is prime using Fermat test."
  (define (try-it a)
    "Determine if A^N mod N = A."
    (= (expmod a n n) a))
  (try-it (+ 1 (random (- n 1)))))

(define (fast-prime? n times)
  "Determine if number N is prime by running `fermat-test' on it TIMES times."
  (cond ((= times 0) #t)
        ((fermat-test n) (fast-prime? n (- times 1)))
        (else #f)))

; Exercise 1.21
; (smallest-divisor 199) => 199
; (smallest-divisor 1999) => 1999
; (smallest-divisor 19999) => 7

; Exercise 1.22
(define (runtime)
  "Stub method for determining uptime of interpreter."
  (tms:clock (times)))

(define (report-prime elapsed-time)
  "Display time taken to do prime test.

ELAPSED-TIME is the time to display."
  (display " *** ")
  (display elapsed-time))

(define (start-prime-test n start-time)
  "Test if number N is prime and report the difference between the current time and START-TIME."
  (when (prime? n)
      (report-prime (- (runtime) start-time))))

(define (timed-prime-test n)
  "Test if number N is prime."
  (newline)
  (display n)
  (start-prime-test n (runtime)))

(define (search-for-primes a b)
  "Search for prime numbers between A and B."
  (define (search-helper i start-time)
    "Helper method for `search-for-primes'."
    (when (< i b)
      (timed-prime-test i)
      (search-helper (+ i 2) start-time)))
  (if (and (>= a 0) (> b a))
      (if (odd? a)
          (search-helper a (runtime))
          (search-helper (+ a 1) (runtime)))
      (error "Invalid input")))

; 1009, 1013, 1019
; 10007, 10009, 10037
; 100003, 100019, 100043
; 1000003, 1000033, 1000037

; Exercise 1.23
(define (next n)
  "Return the next odd number after N."
  (if (= n 2)
      3
      (+ n 2)))

(define (find-divisor n test-divisor)
  "Find the divisor of number N."
  (cond ((> (square test-divisor) n) n)
        ((divides? test-divisor n) test-divisor)
        (else (find-divisor n (next test-divisor)))))

