(define (list-ref items n)
"Return an element from a list.

ITEMS is the list.

N is the position of the element to return."
(if (= n 0)
    (car items)
    (list-ref (cdr items) (- n 1))))

(define (length items)
  "Return the length of list ITEMS."
  (if (null? items)
      0
      (+ 1 (length (cdr items)))))

(define (length items)
  "Return the length of list ITEMS."
  (define (length-helper n items)
    (if (null? items)
        n
        (length-helper (+ n 1) (cdr items))))
  (length-helper 0 items))

(define (append list1 list2)
  "Return a new list with the contents of LIST2 appended to LIST1."
  (if (null? list1)
      list2
      (cons (car list1) (append (cdr list1) list2))))

; Exercise 2.17
(define (last-pair items)
  "Return the last element of list ITEMS."
  (define (last-pair-helper elt items)
    ""
    (if (null? items)
        elt
        (last-pair-helper (car items) (cdr items))))
  (last-pair-helper (car items) (cdr items)))

; Exercise 2.18
(define (reverse items)
  "Reverse the list ITEMS."
  (if (null? (cdr items))
      (list (car items))
      (append (reverse (cdr items)) (list (car items)))))

; Exercise 2.19
(define us-coins (list 50 25 10 5 1))

(define uk-coins (list 100 50 20 10 5 2 1 0.5))

(define (no-more? coin-values)
  "Return if there are any more elements in list COIN-VALUES."
  (null? coin-values))

(define (first-denomination coin-values)
  "Return the first denomination in list COIN-VALUES."
  (car coin-values))

(define (except-first-denomination coin-values)
  "Return all but the first denomination in list COIN-VALUES."
  (cdr coin-values))

(define (cc amount coin-values)
  (cond ((= amount 0) 1)
        ((or (< amount 0) (no-more? coin-values)) 0)
        (else
         (+ (cc amount
                (except-first-denomination coin-values))
            (cc (- amount
                   (first-denomination coin-values))
                coin-values)))))

